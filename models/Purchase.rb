require 'data_mapper'
require 'dm-sqlite-adapter'
require 'andand'

class Purchase
  include DataMapper::Resource

  property :id,         Serial
  property :payer_id,   String
  property :payment_id, String
  property :status,     String
  property :created_at, DateTime
  property :updated_at, DateTime

  property :error_message, Text
  property :timestamp, String
  property :result, String
  property :severity, String
  property :email, String
  property :firstname, String
  property :lastname, String
  property :address, Text
  property :currency_code, String
  property :amount, Integer
  property :status, String

  def self.new_from( ary )
    newobj = self.new
    newobj.error_message = ary.assoc('L_LONGMESSAGE0').andand.last
    newobj.timestamp = ary.assoc('TIMESTAMP').andand.last
    newobj.result = ary.assoc('ACK').andand.last
    newobj.severity = ary.assoc('L_SEVERITYCODE0').andand.last
    newobj.email = ary.assoc('EMAIL').andand.last
    newobj.firstname = ary.assoc('FIRSTNAME').andand.last
    newobj.lastname = ary.assoc('LASTNAME').andand.last
    newobj.address = [
      ary.assoc('SHIPTONAME').andand.last,
      ary.assoc('SHIPTOSTREET').andand.last,
      ary.assoc('SHIPTOCITY').andand.last,
      ary.assoc('SHIPTOSTATE').andand.last,
      ary.assoc('SHIPTOZIP').andand.last,
      ary.assoc('SHIPTOCOUNTRYNAME').andand.last,
    ].join(", ")
    newobj.currency_code = ary.assoc('PAYMENTREQUEST_0_CURRENCYCODE').andand.last
    newobj.amount = ary.assoc('PAYMENTREQUEST_0_AMT').andand.last
    newobj.status = ary.assoc('CHECKOUTSTATUS').andand.last
    newobj.payer_id = ary.assoc('PAYERID').andand.last
    return newobj
  end
end

