require './lib/PayPalExpressProxy'
require 'rspec'

describe PayPalExpressProxy do
  before( :each ) do
    @mock_http = mock('HTTP', :new => @mock_http, :verify_mode= => true, :use_ssl= => true )
    @ppe_proxy = PayPalExpressProxy.new(
      @mock_http,
      'username',
      'password',
      'signature',
      'success_path',
      'cancel_path',
      'verify_none'
    )
  end

  it "should be instanciate-able" do
    @ppe_proxy.should_not be_nil
  end

  it "should post to paypal" do
    @mock_http.should_receive( :post ).with(
      "/nvp",
      "USER=username&PWD=password&SIGNATURE=signature&VERSION=87"
    )
    @ppe_proxy.post_to_paypal( { } )
  end

  it "should post SetExpressCheckout" do
    mock_request = mock( 'mock_request',
      :env => { 'rack.url_scheme' => 'telnet', 'HTTP_HOST' => 'nowhere' }
    )
    @mock_http.should_receive( :post ).with(
      "/nvp",
      "USER=username&PWD=password&SIGNATURE=signature&VERSION=87&METHOD=SetExpressCheckout&AMT=20&cancelUrl=telnet%3A%2F%2Fnowhere%2Fcancel_path&returnUrl=telnet%3A%2F%2Fnowhere%2Fsuccess_path"
    )
    @ppe_proxy.set_express_checkout( mock_request, 20 )
  end

  it "should post GetExpressCheckoutDetails" do
    @mock_http.should_receive( :post ).with(
      "/nvp",
      "USER=username&PWD=password&SIGNATURE=signature&VERSION=87&METHOD=GetExpressCheckoutDetails&TOKEN=t0ken"
    )
    @ppe_proxy.get_express_checkout_details('t0ken')
  end

  it "should post DoExpressCheckoutPayment" do
    @mock_http.should_receive( :post ).with(
      "/nvp",
      "USER=username&PWD=password&SIGNATURE=signature&VERSION=87&METHOD=DoExpressCheckoutPayment&TOKEN=token&PAYMENTACTION=Sale&PAYERID=payerid&PAYMENTREQUEST_0_AMT=25"
    )
    @ppe_proxy.do_express_checkout_payment( 'token', 'payerid', 25 )
  end

end
