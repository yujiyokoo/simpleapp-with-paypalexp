require './paypalexpress_app'
require 'rspec'
require 'rack/test'
require 'capybara/rspec'

Capybara.app = Sinatra::Application

describe 'PayPalExpress App' do
  include Capybara::DSL

  context "index" do
    it "should say please pay" do
      visit '/'
      page.should have_content( 'Please Pay $10' )
    end

    it "should have 'Pay' button" do
      visit '/'
      page.should have_css("input[src='https://www.paypal.com/en_US/i/btn/btn_xpressCheckout.gif']")
    end
  end

  context "pay" do
    it "should redirect" do
      visit '/pay'
      current_url.should match(/sandbox\.paypal\.com/)
    end
    # TODO: spec to be implemented..
    it "should not redirect if setting express payment fails"
  end

  context "success" do
    # TODO: spec to be implemented..
    it "should show payment details"

    it "should show approve link" do
      visit '/success'
      page.should have_selector("a", :text => 'Approve')
    end
  end

  context "cancel" do
    it "should say 'cancelled'" do
      visit '/cancel'
      page.should have_content( 'Payment cancelled' )
    end
  end

  context "approving" do
    context "when do payment call succeeds" do
      # TODO: spec to be implemented..
      it "should say thanks"
    end
    context "when do payment call fails" do
      it "should say it failed" do
        visit '/approve/random_id'
        page.should have_content( 'Wrong purchase id given' )
      end
    end
  end
end
