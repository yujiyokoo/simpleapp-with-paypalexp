require './models/Purchase'
require 'rspec'

describe Purchase do
  it "should be instanciate-able" do
    Purchase.new.should_not be_nil
  end

  it "should make a new instance from array" do
    purchase = Purchase.new_from( [ ['SHIPTONAME', 'foo'], ['FIRSTNAME', 'bar'] ] )
    purchase.firstname.should == 'bar'
    purchase.address.should match( /foo/ )
  end

  it "should not crash when array element not found in new_from" do
    purchase = Purchase.new_from( [] )
    purchase.lastname.should be_nil
  end
end
