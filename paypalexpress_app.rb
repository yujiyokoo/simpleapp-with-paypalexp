require 'rubygems'
require 'sinatra'
require 'haml'
require 'net/http'

require File.dirname(__FILE__) + '/models/Purchase'
require File.dirname(__FILE__) +'/lib/PayPalExpressProxy'

# TODO: get sinatra autoload gem working

# PayPal Sandbox API Credentials
USERNAME = 'seller_1333114891_biz_api1.gmail.com'
PASSWORD = '1333114912'
SIGNATURE = 'AFcWxV21C7fd0v3bYYYRCpSSRl31A0dtlnzG3TknFPbOGhXcq2WekAVh'

# PayPal Sandbox API URL
PAYPALURI = 'https://api-3t.sandbox.paypal.com/'

# URLs to redirect back from PayPal
CANCEL_PATH = '/cancel'
SUCCESS_PATH = '/success'

# DB 
DBADDRESS = 'sqlite:db/paypalexpress.db'

DataMapper::Logger.new('./log/datamapper.log', :debug)
DataMapper.setup(:default, DBADDRESS)

uri = URI(PAYPALURI)
ppe_proxy = PayPalExpressProxy.new(
  Net::HTTP.new( uri.host, uri.port ),
  USERNAME,
  PASSWORD,
  SIGNATURE,
  SUCCESS_PATH,
  CANCEL_PATH,
  OpenSSL::SSL::VERIFY_NONE
)

get '/' do
  @pagetitle = 'Please Pay $10'
  haml :index
end

get '/pay' do

  res = ppe_proxy.set_express_checkout( request, 10 )

  returnparams = URI.decode_www_form( res.body )
  if( 'Success' == returnparams.assoc('ACK').last )
    redirect to( 'https://www.sandbox.paypal.com/webscr?cmd=_express-checkout&token=' + returnparams.assoc('TOKEN').last )
  else
    @message = 'ERROR - ' + returnparams.assoc('L_LONGMESSAGE0').last
    return res.body
  end
end

# forwarded back from PayPal.
get '/success' do
  res = ppe_proxy.get_express_checkout_details(params['token'])

  @details = URI.decode_www_form( res.body )

  # should get CHECKOUTSTATUS: PaymentActionNotInitiated
  # XXX: should we check for CHECKOUTSTATUS: PaymentActionCompleted?
  @purchase = Purchase.new_from( @details )

  if( @purchase.save )
    @message = "Please check the details and click 'Approve'"
  else
    @message = "Failed to save purchase record."
  end

  @pagetitle = 'Approve Payment'
  haml :success
end

get '/approve/:id' do

  @purchase = Purchase.get( params[:id] )

  if(@purchase.nil?) 
    @pagetitle = 'Payment failed'
    @message = 'Wrong purchase id given'
    @paymentresult = []
    haml :results
  else

    res = ppe_proxy.do_express_checkout_payment( params['token'], @purchase.payer_id, @purchase.amount )

    @paymentresult = URI.decode_www_form( res.body )

    if( @paymentresult.assoc('ACK').last == 'Success' )
      # XXX: should we check PAYMENTINFO_0_ACK?
      @pagetitle = 'Thank you for yor payment'
      @message = 'Payment has been processed. Thank you.'
      haml :results
    else
      @pagetitle = 'Payment failed'
      @message = 'ERROR - ' + @paymentresult.assoc('L_LONGMESSAGE0').last
      haml :results
    end
  end
 
end

get '/cancel' do
  @pagetitle = "Payment cancelled"
  @message = "Payment was cancelled"
  haml :cancel
end

