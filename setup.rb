require 'data_mapper'
require 'dm-sqlite-adapter'
require './models/Purchase'

DataMapper::Logger.new($stdout, :debug)
DataMapper.setup(:default, 'sqlite:db/paypalexpress.db')

DataMapper.auto_migrate!

