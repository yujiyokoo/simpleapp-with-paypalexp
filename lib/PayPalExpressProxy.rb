require 'net/http'

CURRENT_API_VERSION = 87

class PayPalExpressProxy

  def initialize( http, uname, passwd, sig, s_path, c_path, vmode )
    @http = http
    @http.verify_mode = vmode
    @http.use_ssl = true

    @username = uname
    @password = passwd
    @signature = sig
    @success_path = s_path
    @cancel_path = c_path
  end

  def post_to_paypal( parameters )
    @http.post(
      '/nvp',
      URI.encode_www_form(
        {
          :USER => @username,
          :PWD => @password,
          :SIGNATURE => @signature,
          :VERSION => CURRENT_API_VERSION
        }.merge( parameters )
      )
    )

  end

  def set_express_checkout(request, amount)
    baseurl = "#{request.env['rack.url_scheme']}://#{request.env['HTTP_HOST']}" 
    return post_to_paypal( {
        :METHOD => 'SetExpressCheckout',
        :AMT => amount,
        :cancelUrl => baseurl + @cancel_path,
        :returnUrl => baseurl + @success_path
      }
    )
  end

  def get_express_checkout_details(token)
    return post_to_paypal(
      {
        :METHOD => 'GetExpressCheckoutDetails',
        :TOKEN => token
      }
    )
  end

  def do_express_checkout_payment( token, payer_id, amount )
    res = post_to_paypal(
      {
        :METHOD => 'DoExpressCheckoutPayment',
        :TOKEN => token,
        :PAYMENTACTION => 'Sale',
        :PAYERID => payer_id,
        :PAYMENTREQUEST_0_AMT => amount
      }
    )
  end

end


